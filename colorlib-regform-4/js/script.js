var department = $("[name=department] option").detach()
$("[name=faculty]").change(function() {
  var val = $(this).val()
  $("[name=department] option").detach()
  department.filter("." + val).clone().appendTo("[name=department]")
}).change()