<?php

include "db-connection.php";

$nama = $_POST['nama'];   //nama pendaftar
$nrp = $_POST['nrp']; 
$birthday = $_POST['birthday'];
$gender = $_POST['gender'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$faculty = $_POST['faculty'];
$department = $_POST['department'];
$division = $_POST['division'];

mkdir("uploads/".$nama."/");
$dir = "uploads/".$nama."/";
$UploadOk = 1;

$cv = basename($_FILES["cv"]["name"]);
$cv_file = $dir . basename($_FILES["cv"]["name"]); //ini nama filenya
$CVFileType = strtolower(pathinfo($cv_file,PATHINFO_EXTENSION));


$photo = basename($_FILES["photo"]["name"]);
$photo_file = $dir . basename($_FILES["photo"]["name"]); //ini nama filenya
$imageFileType = strtolower(pathinfo($photo_file,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if(isset($_POST["submit"]))
{
  $PhotoCheck = getimagesize($_FILES["photo"]["tmp_name"]);
  $cvCheck = getimagesize($_FILES["cv"]["tmp_name"]);

  if($PhotoCheck !== false)
  {
    echo "Photo is an image - " . $PhotoCheck["mime"] . ".";
    $UploadOk = 1;
  }
  else
  {
    $UploadOk = 0;
    die("Photo is not an image." . mysqli_connect_error());
  }

  if($cvCheck !== false)
  {
    echo "CV is an image - " . $cvCheck["mime"] . ".";
    $UploadOk = 1;
  }
  else
  {
    $UploadOk = 0;
    die("CV is not an image." . mysqli_connect_error());
  }
}

// Check if file already exists
if(file_exists($photo_file))
{
  $UploadOk = 0;
  die("Sorry, photo file already exists." . mysqli_connect_error());  
}

if(file_exists($cv_file))
{
  $UploadOk = 0;
  die("Sorry, cv file already exists." . mysqli_connect_error());  
}

// Check file size
// if($_FILES["photo"]["size"] > 500000) {
//   $UploadOk = 0;
//   die("Sorry, your file is too large." . mysqli_connect_error());
// }

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
{
  $UploadOk = 0;
  die("Sorry, only JPG, JPEG, PNG & GIF Photo files are allowed." . mysqli_connect_error());
}

if($CVFileType != "jpg" && $CVFileType != "png" && $CVFileType != "jpeg" && $CVFileType != "gif" && $CVFileType != "pdf")
{
  $UploadOk = 0;
  die("Sorry, only JPG, JPEG, PNG, GIF, & PDF CV files are allowed." . mysqli_connect_error());
}


// Check if $UploadOk is set to 0 by an error
if($UploadOk == 0) die("Sorry, your file was not uploaded." . mysqli_connect_error());

// if everything is ok, try to upload file
else
{
  // Photo
  if(move_uploaded_file($_FILES["photo"]["tmp_name"], $photo_file))
    echo "The file ". basename( $_FILES["photo"]["name"]). " has been uploaded.";
  else 
    die("Sorry, there was an error uploading your file.");

  // CV
  if(move_uploaded_file($_FILES["cv"]["tmp_name"], $cv_file))
    echo "The file ". basename( $_FILES["cv"]["name"]). " has been uploaded.";
  else 
    die("Sorry, there was an error uploading your file.");
}


//INSERT TO DATABASE
$sql = "INSERT INTO pendaftar (nama, nrp, birthday, gender, email, phone, faculty, department, division, cv, photo) 
        VALUES ('$nama', '$nrp', '$birthday', '$gender', '$email', '$phone', '$faculty', '$department', '$division', '$cv', '$photo')";

if(mysqli_query($conn, $sql)) 
  header('Location: index.php');
else
{
  echo "Error: " . mysqli_error($conn) . "<br>";
  echo "<a href='index.php'>kembali ke halaman utama</a>";
}