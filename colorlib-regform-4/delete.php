<?php

include "db-connection.php";

$id_pendaftar = $_POST['id_pendaftar']; //ini kalo pake form
// $id_pendaftar = $_GET['id_pendaftar']; //ini kalo pak button

$sqlFolder = "SELECT nama FROM pendaftar WHERE id_pendaftar=$id_pendaftar";
$result = $conn->query($sqlFolder);
$data = [];

if ($result->num_rows > 0)
{
  while($row = $result->fetch_assoc())
  {
    $data[] = $row;
  }
}
else die("NULL");

foreach($data as $namafolder)
{
  $foldername = "uploads/" . $namafolder['nama']. "/";
  $foldername = '"' . $foldername . '"';
}

function delete_directory($dirname)
{
  if (is_dir($dirname)) $dir_handle = opendir($dirname);
  
  if (!$dir_handle) return false;
  
  while($file = readdir($dir_handle))
  {
    if ($file != "." && $file != "..")
    {
      if (!is_dir($dirname."/".$file)) unlink($dirname."/".$file);
      
      else
        delete_directory($dirname.'/'.$file);
    }
  }
  closedir($dir_handle);
  rmdir($dirname);
  return true;
}

if (!delete_directory($foldername)) {  
  die("$foldername cannot be deleted due to an error");  
}  
else {  
  echo ("$foldername has been deleted");  
} 

$sql = "DELETE FROM pendaftar WHERE id_pendaftar=$id_pendaftar";

if (mysqli_query($conn, $sql)) {
    header('Location: data-pendaftar.php');
} else {
    echo "Error: " . mysqli_error($conn) . "<br>";
    echo "<a href='index.php'>kembali ke halaman utama</a>";
}