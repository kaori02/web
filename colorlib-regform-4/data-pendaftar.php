<?php
include "get-all.php"
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="test.css">
    <title>Applicant List</title>
</head>

<body>
  <div class="container">
  <br><br><br>
  <h2>Applicant List</h2>
  <br>
  <a href="index.php" target="blank" style="text-align: left"><button type="button" class="btn btn-primary">Tambah Data</button></a>
  <br>
  <table class="table table-bordered table-dark table-hover">
    <tr class="">
      <th>NO.</th>
      <th>Name</th>
      <th>NRP</th>
      <th>Birthday</th>
      <th>Gender</th>
      <th>Email</th>
      <th>Phone Number</th>
      <th>Faculty</th>
      <th>Department</th>
      <th>Division</th>
      <th>CV</th>
      <th>Photo</th>
      <th>ACTION</th>
    </tr>

    <?php
      //----------------------------------------------nampilin data---------------------------------------------------------------
      $i = 1;
      foreach ($data as $pendaftar) :?>
        <tr>
          <td style="text-align: center"><?= $i++ ?></td>
          <td><?= $pendaftar['nama'] ?></td>
          <td><?= $pendaftar['nrp'] ?></td>
          <td><?= $pendaftar['birthday'] ?></td>
          <td><?= $pendaftar['gender'] ?></td>
          <td><?= $pendaftar['email'] ?></td>
          <td><?= $pendaftar['phone'] ?></td>
          <td><?= $pendaftar['faculty'] ?></td>
          <td><?= $pendaftar['department'] ?></td>
          <td><?= $pendaftar['division'] ?></td>

          <td><a href="<?= "uploads/" . $pendaftar['nama']. "/". $pendaftar['cv']?>" download="<?= "uploads/" . $pendaftar['nama']. "/". $pendaftar['cv']?>">Download</a></td>
          <td style="width:500px;height:500px"><a href="<?= "uploads/" . $pendaftar['nama']. "/". $pendaftar['photo']?>"><img style="width=100%" src="<?= "uploads/" . $pendaftar['nama']. "/". $pendaftar['photo']?>" alt=""></a></td>
          
          <td>       
            <div class="row">
              <div class="col">
                <a href='edit.php?id_pendaftar=<?= $pendaftar['id_pendaftar'] ?>'><button class="btn btn-block btn-primary">edit</button></a>
              </div>
              
              <div class="col">
                <form action='delete.php' method='post'>
                  <input type='hidden' name='id_pendaftar' value='<?= $pendaftar['id_pendaftar'] ?>'>
                  <button type='submit' class="btn btn-block btn-danger">delete</button>
                </form>
              </div>
            </div>
          </td>
        </tr>
        <!-------------------------------------------- end nampilin data --------------------->
        <?php endforeach?>
    </table>
  </div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>