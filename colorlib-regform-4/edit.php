<?php
include "db-connection.php";

$id_pendaftar = $_GET['id_pendaftar'];
$sql = "SELECT * FROM pendaftar WHERE id_pendaftar='$id_pendaftar' LIMIT 1";
$result = $conn->query($sql);

if ($result->num_rows > 0) $pendaftar = $result->fetch_assoc();

$conn->close();
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags-->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Registration Form">
  <meta name="author" content="Ichiro">
  <meta name="keywords" content="Registration Form">

  <!-- Title Page-->
  <title>Registration Form</title>

  <!-- Icons font CSS-->
  <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
  <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
  <!-- Font special for pages-->
  <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Vendor CSS-->
  <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
  <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

  <!-- Main CSS-->
  <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
  <div class="page-wrapper bg-blue p-t-130 p-b-100 font-poppins">
    <div class="wrapper wrapper--w680">
      <div class="card card-4">
        <div class="card-body">
          <h2 class="title">Registration Form</h2>

          <form action="store.php" method="post" enctype="multipart/form-data">
            
            <input type="hidden" name="id" value="<?= $pendaftar['id_pendaftar'] ?>">
            
            <div class="row row-space">
              <div class="col-2">
                <div class="input-group">
                  <label for="nama">Name</label>
                  <input class="input--style-4" type="text" name="nama" value="<?= $pendaftar['nama'] ?>">
                </div>
              </div>
              <div class="col-2">
                <div class="input-group">
                  <label for="nrp">NRP</label>
                  <input class="input--style-4" type="text" name="nrp" value="<?= $pendaftar['nrp'] ?>">
                </div>
              </div>
            </div>

            <div class="row row-space">
              <div class="col-2">
                <div class="input-group">
                  <label for="birthday">Birthday</label>
                  <div class="input-group-icon">
                    <input class="input--style-4 js-datepicker" type="text" name="birthday" value="<?= $pendaftar['birthday'] ?>">
                    <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                  </div>
                </div>
              </div>

              <div class="col-2">
                <div class="input-group">
                  <label for="gender">Gender</label>
                  <div class="p-t-10">
                    <label class="radio-container m-r-45">Male
                      <input type="radio" name="gender" value="<?= $pendaftar['gender'] ?>">
                      <span class="checkmark"></span>
                    </label>
                    <label class="radio-container">Female
                      <input type="radio" name="gender" value="<?= $pendaftar['gender'] ?>">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>

            <div class="row row-space">
              <div class="col-2">
                <div class="input-group">
                  <label for="email">Email</label>
                  <input class="input--style-4" type="email" name="email" value="<?= $pendaftar['email'] ?>">
                </div>
              </div>

              <div class="col-2">
                <div class="input-group">
                  <label for="phone">Phone Number</label>
                  <input class="input--style-4" type="text" name="phone" value="<?= $pendaftar['phone'] ?>">
                </div>
              </div>
            </div>

            <div class="row row-space">
              <div class="col-2">
                <div class="input-group">
                  <label class="label">Faculty</label>
                  <div class="rs-select2 js-select-simple select--no-search">
                    <select id="faculty" name="faculty" value="<?= $pendaftar['faculty'] ?>">
                      <option value="" disabled="disabled" selected="selected">Choose Faculty</option>
                      <option value="fsad">SCIENTICS</option>
                      <option value="ftirs">INDSYS</option>
                      <option value="ftspk">CIVPLAN</option>
                      <option value="ftk">MARTECH</option>
                      <option value="fteic">ELECTICS</option>
                      <option value="fdkbd">CREABIZ</option>
                      <option value="fv">VOCATION</option>
                    </select>
                    <div class="select-dropdown"></div>
                  </div>
                </div>
              </div>

              <div class="col-2">
                <div class="input-group">
                  <label class="label">Department</label>
                  <div class="rs-select2 js-select-simple select--no-search">
        
                    <select name="department">
                      <option class="fsad" value="" disabled="disabled" selected="selected">Choose Department</option>
                      <option class="fsad" value="fisika">Physics</option>
                      <option class="fsad" value="matematika">Mathematics</option>
                      <option class="fsad" value="statistika">Statistics</option>
                      <option class="fsad" value="kimia">Chemistry</option>
                      <option class="fsad" value="biologi">Biology</option>
                      <option class="fsad" value="aktuaria">Actuarial Science</option>
                
                      <option class="ftirs" value="" disabled="disabled" selected="selected">Choose Department</option>
                      <option class="ftirs" value="mesin">Mechanical Engineering</option>
                      <option class="ftirs" value="tekkim">Chemical Engineering</option>
                      <option class="ftirs" value="tekfis">Engineering Physics</option>
                      <option class="ftirs" value="industri">Industrial Engineering</option>
                      <option class="ftirs" value="mamet">Material and Metallurgical Engineering</option>

                      <option class="ftspk" value="" disabled="disabled" selected="selected">Choose Department</option>
                      <option class="ftspk" value="sipil">Civil Engineering</option>
                      <option class="ftspk" value="arsi">Architecture</option>
                      <option class="ftspk" value="tekling">Environmental Engineering</option>
                      <option class="ftspk" value="pwk">Regional and Urban Planning</option>
                      <option class="ftspk" value="geomat">Geomatics Engineering</option>
                      <option class="ftspk" value="geofis">Geophysics Engineering</option>

                      <option class="ftk" value="" disabled="disabled" selected="selected">Choose Department</option>
                      <option class="ftk" value="tekpal">Naval Architecture and Shipbuilding Engineering</option>
                      <option class="ftk" value="siskal">Marine Engineering</option>
                      <option class="ftk" value="tekla">Ocean Engineering</option>
                      <option class="ftk" value="seatrans">Sea Transportation Engineering</option>
                      
                      <option class="fteic" value="" disabled="disabled" selected="selected">Choose Department</option>
                      <option class="fteic" value="elektro">Electrical Engineering</option>
                      <option class="fteic" value="biomedik">Biomedical Engineering</option>
                      <option class="fteic" value="tekkom">Computer Engineering</option>
                      <option class="fteic" value="tc">Informatics</option>
                      <option class="fteic" value="si">Information Systems</option>
                      <option class="fteic" value="it">Information Technology</option>

                      <option class="fdkbd" value="" disabled="disabled" selected="selected">Choose Department</option>
                      <option class="fdkbd" value="despro">Industrial Product Design</option>
                      <option class="fdkbd" value="desin">Interior Design</option>
                      <option class="fdkbd" value="dkv">Visual Communication Design</option>
                      <option class="fdkbd" value="manbis">Business Management</option>
                      <option class="fdkbd" value="studpem">Developmental Studies</option>

                      <option class="fv" value="" disabled="disabled" selected="selected">Choose Department</option>
                      <option class="fv" value="d4sipil">Civil Infrastructure Engineering</option>
                      <option class="fv" value="d3mits">Industrial Mechanical Engineering</option>
                      <option class="fv" value="dtektro">Automation Electronic Engineering</option>
                      <option class="fv" value="d3kim">Industrial Chemical Engineering</option>
                      <option class="fv" value="instrumen">Instrumentation Engineering</option>
                      <option class="fv" value="stabis">Business Statistics</option>
                    </select>
                    <div class="select-dropdown"></div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="row row-space">
              <div class="col-2">
                <div class="input-group">
                  <label for="cv">Upload CV</label>

                  <div class="value">
                    <div class="input-group js-input-file">
                      <input class="input-file" type="file" name="cv" id="cv">
                      <label for="cv">Choose file</label>
                      <span class="input-file__info">No file chosen</span>
                    </div>

                    <div class="label--desc">Maximum size 1 MB</div>
                  </div>

                </div>
              </div>

              <div class="col-2">
                <div class="input-group">
                  <label for="photo">Upload Photo</label>

                  <div class="value">
                    <div class="input-group js-input-file">
                      <input class="input-file" type="file" name="photo" id="photo">
                      <label for="photo">Choose file</label>
                      <span class="input-file__info">No file chosen</span>
                    </div>

                    <div class="label--desc">Maximum size 1 MB</div>
                  </div>
                </div>
              </div>
            </div>
            <br>

            <div class="input-group">
              <label for="division">Division</label>
              <div class="rs-select2 js-select-simple select--no-search">
                <select name="division">
                  <option value="" disabled="disabled" selected="selected">Choose Division</option>
                  <option value="programming">Programming</option>
                  <option value="electronic">Electronic</option>
                  <option value="mechanic">Mechanic</option>
                  <option value="official">Official</option>
                </select>
                <div class="select-dropdown"></div>
              </div>
            </div>

            <div class="p-t-15">
              <button class="btn btn--radius-2 btn--blue" type="submit">Submit</button>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Jquery JS-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <!-- Vendor JS-->
  <script src="vendor/select2/select2.min.js"></script>
  <script src="vendor/datepicker/moment.min.js"></script>
  <script src="vendor/datepicker/daterangepicker.js"></script>

  <!-- Main JS-->
  <script src="js/global.js"></script>
  <script src="js/script.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->