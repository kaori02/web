<?php

include "db-connection.php";

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["foto"]["name"]); //ini nama filenya
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
  $check = getimagesize($_FILES["foto"]["tmp_name"]);
  if($check !== false) {
      echo "File is an image - " . $check["mime"] . ".";
      $uploadOk = 1;
  } 
  else {
    $uploadOk = 0;
    die("File is not an image." . mysqli_connect_error());  

  }
}
// Check if file already exists
if (file_exists($target_file)) {
  $uploadOk = 0;
  die("Sorry, file already exists." . mysqli_connect_error());  
}

// Check file size
// if ($_FILES["foto"]["size"] > 500000) {
//   $uploadOk = 0;
//   die("Sorry, your file is too large." . mysqli_connect_error());
// }

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
{
  $uploadOk = 0;
  die("Sorry, only JPG, JPEG, PNG & GIF files are allowed." . mysqli_connect_error());

}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) die("Sorry, your file was not uploaded." . mysqli_connect_error());

// if everything is ok, try to upload file
else
{
  if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) echo "The file ". basename( $_FILES["foto"]["name"]). " has been uploaded.";
  else echo "Sorry, there was an error uploading your file.";
}

$nama = $_POST['nama'];
$nrp = $_POST['nrp'];
$alamat = $_POST['alamat'];
$foto = basename($_FILES["foto"]["name"]);

$sql = "INSERT INTO mahasiswa (nama, nrp, alamat, foto) VALUES ('$nama', '$nrp', '$alamat', '$foto')";

if (mysqli_query($conn, $sql)) header('Location: index.php');
else {
  echo "Error: " . mysqli_error($conn) . "<br>";
  echo "<a href='index.php'>kembali ke halaman utama</a>";
}