# web

Learn to build my own web

> NOTE : Clone this repository to your `\xampp\htdocs\` folder if you want to run the `php` file in your local machine
## website template

- [webhost indo](https://id.000webhost.com/template/semua/1?__cf_chl_jschl_tk__=20dc34bbd60e6e709cb82782036df97f5caea3f6-1582398789-0-Af5oz1Z5lEdDfHJqwcLduGZl7xf4Ft59yWCqlfygtdP0I5ZFzP6_qBjHU-bJ_lQhz31e69wJ34t0aUhbzwvsVfTlOQQKEIq39hyFBjiE-di4AumFxTL_aM_cWl_IqSjzQqE4cN2RQQ-HaaPvuUOAi516m8LuUab5rS4IpJsiYIezasT1nUEHnVdseWIzqnUBCAWGEkhvEI6YHDBIhXVZRTEJdEsEov45ITk9ld2V3FXLuNmbGDHbTU6cVMKD3Qhe5nsUMrYdSw1b3zgzIUrsIJVVOA1Az6HqkQnjIIKm1kh0)
- [bootstraps made](https://bootstrapmade.com/)
- [free css **(recommended)**](https://www.free-css.com/free-css-templates)
- [start bootstrap](https://startbootstrap.com/themes/agency/)
- [templated](https://templated.co/)
- [free website template](https://freewebsitetemplates.com/)
- [skt theme (php)](https://www.sktthemes.org/shop/free-hotel-booking-wordpress-theme/)
- [template memo](https://templatemo.com/tm-544-comparto)
- [article dumet school](https://www.dumetschool.com/blog/20-Free-Templates-HTML-CSS-Siap-Pakai)
- [too plate](https://www.tooplate.com/free-templates)
- [article niagahoster](https://www.niagahoster.co.id/blog/template-web-gratis/)
- [html 5 up](https://html5up.net/)
- [colorlib **(recommended)**](https://colorlib.com/wp/templates/)
- [colorlib article **(recommended)**](https://colorlib.com/wp/free-css-website-templates/)
- [mobirise **MY LOVELY ONE**](https://mobirise.com/html-templates/)
