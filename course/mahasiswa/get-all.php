<?php

include "db-connection.php";

$sql = "SELECT * FROM mahasiswa";
$result = $conn->query($sql);
$data = [];

if ($result->num_rows > 0)
{
  // output data of each row
  while($row = $result->fetch_assoc())
  {
    // $data[] = ['id' => $row["id"], 'nama' => $row['nama'], 'nrp' => $row['nrp'], 'alamat' => $row['alamat']];
    $data[] = $row;
  }
}

$conn->close();