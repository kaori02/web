<?php
include "db-connection.php";

$id = $_GET['id'];
$sql = "SELECT * FROM mahasiswa WHERE id='$id' LIMIT 1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // $row = $result->fetch_assoc();
    $mahasiswa = $result->fetch_assoc();;//['id' => $row["id"], 'nama' => $row['nama'], 'nrp' => $row['nrp'], 'alamat' => $row['alamat']];
}

$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Edit Data Mahasiswa</title>
  </head>

  <body>
    <div class="container">
      <h2>Data Mahasiswa</h2>      
      <form action="store.php" method="post">
        <input type="hidden" name="id" value="<?= $mahasiswa['id'] ?>">
        
        <div class="form-group">
          <label for="nama">Nama :</label>
          <input type="text" class="form-control" name="nama" value="<?= $mahasiswa['nama'] ?>">
        </div>
        
        <div class="form-group">
          <label for="nrp">NRP :</label>
          <input type="number" class="form-control" name="nrp" value="<?= $mahasiswa['nrp'] ?>">
        </div>
        
        <div class="form-group">
          <label for="alamat">Alamat :</label>
          <textarea name="alamat" class="form-control"><?= $mahasiswa['alamat'] ?></textarea><br>
        </div>
        
        <button type="submit" class="btn btn-block btn-primary">Simpan</button>
        <a href="index.php"><button type="button" class="btn btn-block btn-outline-secondary">Batal</button></a>
        
      </form> 
    </div>
  </body>
  
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>