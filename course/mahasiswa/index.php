<?php
include "get-all.php"
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Data Mahasiswa</title>
</head>

<body>
  <div class="container">
  <br><br><br>
  <h2>Data Mahasiswa</h2>
  <br>
  <a href="create.php" style="text-align: left"><button type="button" class="btn btn-primary">Tambah Data</button></a>
  <br>
  <table class="table table-bordered table-dark table-hover">
    <tr class="">
      <th>NO.</th>
      <th>NAMA</th>
      <th>NRP</th>
      <th>ALAMAT</th>
      <th>ACTION</th>
    </tr>
    <?php
      $i = 1;
      foreach ($data as $mahasiswa) :?>
        <tr>
          <td style="text-align: center"><?= $i++ ?></td>
          <td><?= $mahasiswa['nama'] ?></td>
          <td><?= $mahasiswa['nrp'] ?></td>
          <td><?= $mahasiswa['alamat'] ?></td>
          <td>
        
            <div class="row">
              <div class="col">
                <a href='edit.php?id=<?= $mahasiswa['id'] ?>'><button class="btn btn-block btn-primary">edit</button></a>
              </div>
              
              <div class="col">
                <form action='delete.php' method='post'>
                  <input type='hidden' name='id' value='<?= $mahasiswa['id'] ?>'>
                  <button type='submit' class="btn btn-block btn-danger">hapus</button>
                </form>
              </div>
            </div>
            
            <!-- 
            <a href='delete.php?id=".$mahasiswa['id']."'><button class=\"btn btn-danger\">hapus</button></a>
            -->
          </td>
        </tr>
        <?php endforeach?>
    </table>
  </div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>